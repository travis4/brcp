<?php
	//should probably try to move this to the ZeusIntercepter to make it look cleaner.
	class User extends Model
	{
		//private $salt = "LaJIa37azfb4umxf2sCg";
		public function __construct()
		{
			if(func_num_args() > 0)
			{
				parent::__construct(func_get_args());
			}
			else
			{
				parent::__construct();
			}
			$this->add_before_filter("validates_password");
			$this->add_before_filter("validates_email");
			$this->validates_presence_of("login");
			$this->validates_presence_of("first_name");
			$this->validates_presence_of("last_name");
			$this->validates_presence_of("email");
			$this->validates_presence_of("domain");
		}
		
		public function sign_in()
		{
			//return $this->where(["login"=>$_POST["username"],"password"=>password_hash($_POST["password"],PASSWORD_BCRYPT,['salt'=>$this->salt])])->execute();
			$_SESSION["raw_password"] = $_POST["password"];
			return new User($this->where(["login"=>$_POST["username"],"password"=>md5($_POST["password"])])->execute());
		}
		
		
		public function get_user_from_session()
		{
			return new User($this->where(["session_id"=>session_id()])->execute());
		}
		
		
		public function validates_email()
		{
			if(filter_var($_POST["email"], FILTER_VALIDATE_EMAIL))
			{
				return true;
			}
			else
			{
				$this->add_error("Email address is not valid");
				return false;
			}
		}
		
		public function validates_password()
		{
			global $router;
			if($router->getAction() != "create") return true;
			if($_POST["password"] != $_POST["confirm_password"])
			{
				//die("passwords don't match");
				$this->add_error("Passwords do not match");
				return false;
			}
			return true;
		}
		public function create_system_user()
		{
			//Linux
			$this->create_linux_user();
			//Mac
			//Return true no matter what until we can implement a more sophisticated way.
			return true;
		}
		public function create_linux_user()
		{
			$fp = fopen("/var/brcp/newuser.list","a");
			if(flock($fp,LOCK_EX))
			{
				//exclusive lock
				fwrite($fp,$_POST["username"]." ".$_POST["password"]." ".$_POST["domain"]."\n\r");
				fflush($fp);
				flock($fp,LOCK_UN);
			}
			else
			{
				$this->add_error("Could not get file lock");
			}
		}
		
	}
	
?>
