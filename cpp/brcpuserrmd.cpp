#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <fstream>
int main()
{
        pid_t pid, sid;

        /* Fork off the parent process */
        pid = fork();
        if (pid < 0) {
                        exit(EXIT_FAILURE);
        }
        /* If we got a good PID, then
           we can exit the parent process. */
        if (pid > 0) {
                        exit(EXIT_SUCCESS);
        }

        /* Change the file mode mask */
        umask(0);       

        /* Open any logs here */

        /* Create a new SID for the child process */
        sid = setsid();
        if (sid < 0) {
                        /* Log any failure here */
                        exit(EXIT_FAILURE);
        }

        /* Change the current working directory */
        if ((chdir("/")) < 0) {
                        /* Log any failure here */
                        exit(EXIT_FAILURE);
        }


        /* Close out the standard file descriptors */
        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);

        while(1)
        {
                //ourtask
    			char user[256];
                std::ifstream infile("/var/brcp/deluser.list");
                std::ofstream logfile("/var/brcp/del.log");
                while(infile >> user)
                {
					infile >> user;
					char command[256];
					sprintf(command, "userdel -r %s", user);
					system(command);
					logfile << "Command: " << command << std::endl;
			//      command = char[256];
			//      sprintf(command, "mkdir /home/%s", user);
			//      system(command);
			//      logfile << "Command: " << command << std::endl;
					strcpy(command,"");
					sprintf(command, "rm /etc/httpd/sites-enabled/%s",user);
					system(command);
					logfile << "Command: " << command << std::endl;
					
					char mysqlInit[1024];
					sprintf(mysqlInit,"mysql -u root -e \"DROP USER '%s'@'localhost'; DROP DATABASE %s;\"",user,user);
					system(mysqlInit);
					
					system("service httpd reload");
                }
                infile.close();
                logfile.close();
                std::ofstream ofile("/var/brcp/deluser.list");
                ofile << "";
                ofile.close();
                //sleep for 60 seconds
                sleep(60);
        }
        exit(EXIT_SUCCESS);
}