#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <fstream>
int main()
{
        pid_t pid, sid;

        /* Fork off the parent process */
        pid = fork();
        if (pid < 0) {
                        exit(EXIT_FAILURE);
        }
        /* If we got a good PID, then
           we can exit the parent process. */
        if (pid > 0) {
                        exit(EXIT_SUCCESS);
        }

        /* Change the file mode mask */
        umask(0);       

        /* Open any logs here */

        /* Create a new SID for the child process */
        sid = setsid();
        if (sid < 0) {
                        /* Log any failure here */
                        exit(EXIT_FAILURE);
        }

        /* Change the current working directory */
        if ((chdir("/")) < 0) {
                        /* Log any failure here */
                        exit(EXIT_FAILURE);
        }


        /* Close out the standard file descriptors */
        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);

        while(1)
        {
                //ourtask
                char user[256];
                std::ifstream infile("/var/brcp/newuser.list");
                std::ofstream logfile("/var/brcp/log.log");
                while(infile >> user)
                {
					char domain[256];
					char password[256];
					infile >> password;
					infile >> domain;
					char command[256];
					sprintf(command, "useradd -G rvm %s", user);
					system(command);
					logfile << "Command: " << command << std::endl;
			//      command = char[256];
			//      sprintf(command, "mkdir /home/%s", user);
			//      system(command);
			//      logfile << "Command: " << command << std::endl;
					strcpy(command,"");
					sprintf(command, "echo -e \"%s\n%s\" | passwd %s",password,password,user);
					system(command);
					logfile << "Command: " << command << std::endl;
		
					char userFile[256];
					sprintf(userFile,"/etc/httpd/sites-enabled/%s",user);
					std::ofstream vfile(userFile);	
					vfile << "<VirtualHost *:80>" << std::endl;
					vfile << "\tServerName " << domain << std::endl;
					vfile << "\tServerAlias www." << domain << std::endl;
					vfile << "\tDocumentRoot /home/" << user <<"/public" << std::endl;
					vfile << "\t<Directory  \"/home/"<<user<<"/public\">"<<std::endl;
					vfile << "\t\tOptions FollowSymLinks" << std::endl;
					vfile << "\t\tAllowOverride None" << std::endl;
					vfile << "\t\tOrder allow,deny" << std::endl;
					vfile  << "\t\tAllow from all" << std::endl;
					vfile << "\t\tRequire all granted" << std::endl;
					vfile << "\t</Directory>" << std::endl;
					vfile << "</VirtualHost>" << std::endl;
					vfile.close();
					
					char mysqlInit[1024];
					sprintf(mysqlInit,"mysql -u root -e \"CREATE DATABASE %s; GRANT ALL PRIVILEGES ON %s.* To '%s'@'localhost' IDENTIFIED BY '%s';\"",user,user,user,password);
					system(mysqlInit);
					
					char aclCmds[1024];
					sprintf(aclCmds,"chmod -R 0700 /home/%s/\nsetfacl -R -m u:apache:rx /home/%s/",user,user);
					system(aclCmds);
					system("service httpd reload");
                }
                infile.close();
                logfile.close();
                std::ofstream ofile("/var/brcp/newuser.list");
                ofile << "";
                ofile.close();
                //sleep for 60 seconds
                sleep(60);
        }
        exit(EXIT_SUCCESS);
}
