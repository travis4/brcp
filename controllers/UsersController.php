<?php

class UsersController extends ApplicationController
{

	public function __construct()
	{
		parent::__construct();
	}
	
	public function user_panel()
	{
		$this->check_login();
		$this->renderLayoutView();
	}
	
	public function index()
	{
		//this is a function to check the login
		/*Note: I had no way to determine the variable user dynamically in Controller.php without giving it
		to an array (similar to CI) therefore $this->data will be used as a convention in array.  It can be used as $user in view*/
		global $config;
		if(isset($this->current_user) && $this->current_user->login != null)
		{
			switch($this->current_user->user_level)
			{
				case 2:
					header("Location: ".$config["base_uri"]."user/admin_panel");
					break;
				default:
					header("Location: ".$config["base_uri"]."user/user_panel");
			}
		}
		else
		{
			$this->data['user'] = new User();
			$this->renderView($this->data);
		}
	}
	
	public function webconsole()
	{
		$this->renderPathView("../external_apps/webconsole/webconsole.php");
	}
	
	public function backup_data()
	{

		global $config;
		$file = "/tmp/".$this->current_user->accessibleAttributes["login"].".tar";
		if(file_exists($file))
		{
			unlink($file);
		}
		if(file_exists($file.".gz"))
		{
			unlink($file.".gz");
		}
		//This is to create a .zip backup of /public/ and the database
		include_once('../lib/Ifsnop/Mysqldump/Mysqldump.php');
		$dump = new Ifsnop\Mysqldump\Mysqldump( $this->current_user->accessibleAttributes["login"], $config["mysql_root_name"], $config["mysql_root_password"], 'localhost', 'mysql', array() );
		$dump->start("/tmp/".$this->current_user->accessibleAttributes["login"].".sql");
		
		//zip public and the dump in /tmp/<username>.sql
		$tar = new PharData($file);
		
		$tar->buildFromDirectory("/home/".$this->current_user->accessibleAttributes["login"]."/public/");
		$tar->addFile("/tmp/".$this->current_user->accessibleAttributes["login"].".sql","mysql_dump.sql");
		//$tar->compress(Phar::GZ);;
		if(filesize($file) <= 0)
		{
			die("Bad filesize");
		}
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename='.basename($file));
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		ob_flush();
		flush();
		ob_end_clean();
		readfile($file);
		ob_flush();
		flush();
	}
	
	private function dirSize($directory) {
		$size = 0;
		foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory)) as $file){
			$size+=$file->getSize();
		}
		return $size;
	} 
	
	public function verify_login_json()
	{
		if(!isset($_POST["session_id"]))
		{
			echo json_encode(["error"=>"Could not read a posted session_id"]);
			die();
		}
		$user = new User((new User)->where(["session_id"=>$_POST["session_id"]])->execute()[0]);
		//die(var_dump($user));
		if(isset($user->accessibleAttributes["login"]))
		{
			echo json_encode(["logged_in"=>true,"login"=>$this->current_user->accessibleAttributes["login"], "user_level"=>$this->current_user->accessibleAttributes["user_level"]]);
		}
		else
		{
			echo json_encode(["logged_in"=>false]);
		}
	}
	public function admin_panel()
	{
		$this->renderLayoutView();
	}
	
	public function sign_in()
	{
		session_regenerate_id(true);
		$user = new User();
		$this->data["user"] = $user->sign_in();
		if(isset($this->data["user"]->accessibleAttributes["login"]))
		{
			$this->data["user"]->accessibleAttributes["session_id"] = session_id();
			if($this->data["user"]->update())
			{
				if($this->data["user"]->accessibleAttributes["user_level"] == 2)
				{
					$this->redirect("admin_panel");
				}
				else
				{
					$this->redirect("user_panel");
				}
			}
			else
			{
				$this->render("index",["alert"=>"Problem updating DB."]);
			}
		}
		else
		{
			$this->render("index",array("alert"=>"Invalid user name or password."));
		}
	}
	
	
	public function sign_out()
	{
		$this->current_user->accessibleAttributes["session_id"] = NULL;
		$this->current_user->update();
		$this->redirect("./index");
	}
	
	public function new_user()
	{
		if(!isset($this->data))
		{
			$this->data["user"] = new User();
		}
		$this->renderLayoutView($this->data);
	}
	
	public function create()
	{
		$this->data["user"] = new User(["login"=>$_POST["username"],"first_name"=>$_POST["first_name"],"last_name"=>$_POST["last_name"],
		"email"=>$_POST["email"],"password"=>md5($_POST["password"]),"user_level"=>$_POST["level"],"domain"=>$_POST["domain"]]);
		if($this->data["user"]->save() && $this->data["user"]->create_system_user())
		{
			$this->render("new_user",["notice"=>"Successfully created user."]);
		}
		else
		{
			$this->render("new_user",["alert"=>"Failed to create user."]);
		}
	}
	
	public function manage()
	{
		$this->data["user"] = new User();
		$this->data["users"] = $this->data["user"]->all();
		$this->renderLayoutView($this->data);
	}
	
	public function show_log()
	{
		echo "<pre>".file_get_contents("/home/".$this->current_user->accessibleAttributes["login"]."/log/production.log")."</pre>";
	}
	
	public function filemanager_sign_in()
	{
		global $config;
		$prefix = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
	  	$port = isset($_SERVER["SERVER_PORT"]) ? $_SERVER["SERVER_PORT"] : 80;
		$_SESSION['file_mode']='brcp';
		$_SESSION['credentials_brcp']['username'] = session_id();
		$_SESSION['credentials_brcp']['domain'] = $prefix.$_SERVER["SERVER_NAME"].":$port".$config["base_uri"];
		$GLOBALS["home_dir"] = "/home/".$this->current_user->accessibleAttributes["login"];
		if($this->current_user->accessibleAttributes["user_level"] == 2)
		{
			$GLOBALS["home_dir"]	= $_SERVER["DOCUMENT_ROOT"];
		}
		//$GLOBALS["home_url"]	= $this->data[3];
		$GLOBALS["show_hidden"]	= 'Y';
		//$GLOBALS["no_access"]	= $this->data[5];
		$GLOBALS["permissions"]	= 7;
		header("location:../external_apps/filemanager/");
	}
	
	public function delete()
	{
		$this->data["user"] = new User();
		$this->data["user"]->where(["id"=>$_POST["user_id"]]);
		$this->data["user"]->delete();
		$this->redirect("./manage");
	}
	
	
}
