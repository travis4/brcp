<?php


class PagesController extends Controller
{
	public function __construct()
	{
		parent::__construct();
		set_error_handler("pages_override_zeus_handler");
	}
	
}

function pages_override_zeus_handler()
{
	global $controller_instance;
	global $zeus_uri;
	global $router;
	$controller_instance->renderLayoutView();
}
