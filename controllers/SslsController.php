<?php

class SslsController extends ApplicationController
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$sslDir = "/home/".$this->current_user->login."/ssl";
		$private = "";
		$public = "";
		$csr = "";
		if(file_exists($sslDir."/private.key"))
		{
			$private = file_get_contents($sslDir."/private.key");
		}
		if(file_exists($sslDir."/public.key"))
		{
			$public = file_get_contents($sslDir."/public.key");
		}
		if(file_exists($sslDir."/request.csr"))
		{
			$csr = file_get_contents($sslDir."/request.csr");
		}
		switch($this->format())
		{
			case "json": 
				echo json_encode(array("alert"=>"not implemented"));
				break;
			default:
				$this->renderLayoutView(["public"=>$public,"private"=>$private,"csr"=>$csr]);
		}
	}

	public function generate_public_private_keys()
	{
		$keys = openssl_pkey_new();
		openssl_pkey_export($keys,$private);
		$public = openssl_pkey_get_details($keys);
		$public = $public["key"];
		$sslDir = "/home/".$this->current_user->login."/ssl";
		if(!file_exists($sslDir))
		{
			mkdir($sslDir);
		}
		file_put_contents($sslDir."/private.key",$private);
		file_put_contents($sslDir."/public.key",$public);
		switch($this->format())
		{
			case "json":
				echo json_encode(["private_key"=>$private,"public_key"=>$public]);
			break;
			default:
				//remove later
				echo json_encode(["private_key"=>$private,"public_key"=>$public]);
				//$this->renderLayoutView();
		}
	}

	public function generate_csr()
	{
		$sslDir = "/home/".$this->current_user->login."/ssl";
		$dn = array("countryName"=>$_POST["countryCode"],
				"stateOrProvinceName" => $_POST["state"],
				"localityName"=>$_POST["locality"],
				"organizationName"=>$_POST["organization"],
				"commonName"=>$_POST["commonName"],
				"emailAddress"=>$_POST["email"]);

		$privateKey = $_POST["privateKey"];
		$csr = openssl_csr_new($dn,$privateKey);
		openssl_csr_export($csr,$csrout);
		file_put_contents($sslDir."/request.csr",$csrout);
		echo json_encode(array("csr"=>$csrout));	
	}
}
