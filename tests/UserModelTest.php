<?php
#To prevent user error in User module
class UserModelTest extends TestUnit
{
	public function __construct()
	{
		echo "<strong>User Model Test</strong><br />";
		$this->setup_vars();
		echo $this->assert_equals("POST username should be set",$_POST["username"],"admin");
		echo $this->assert_equals("POST password should be set",$_POST["password"],"admin");
		echo $this->assert_not_null("login in user object should not be null",$this->obj->accessibleAttributes["login"]);
		echo $this->assert_equals("login should match user object",$this->obj->accessibleAttributes["login"],"admin");
		echo $this->assert_equals("newuser should have errors",(count($this->newUser->get_errors()) > 0),true);
		echo $this->assert_equals("magic get should match accessible array",$this->obj->accessibleAttributes["login"],$this->obj->login);
		echo "<hr />";
	}
	
	public function setup_vars()
	{
		
		$_POST["username"] = "admin";
		$_POST["password"] = "admin";
		$_POST["confirm_password"] = "abc123";
		$user = new User();
		$this->obj = $user->sign_in();
		@$this->establish_fake_route("/user/create");
		$this->newUser = new User();
		$this->newUser->save();
	}
	
}