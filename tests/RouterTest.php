<?php
//include "../system/router.php";
class RouterTest extends TestUnit
{
	public function __construct()
	{
		echo "<strong>Router Test</strong><br />";
		$this->setupVars();
		echo $this->assert_equals("controllers are equal",$this->router->getController(),"UsersController");
		echo $this->assert_equals("Actions are equal",$this->router->getAction(),"show");
		echo $this->assert_equals("Formats are equal",$this->router->getFormat(),"json");
		echo "<hr />";
	}
	
	public function setupVars()
	{
		$route = "user/show.json";
		$this->router = new Router($route);
	}
}