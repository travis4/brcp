<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>BRCP Sign In</title>

   <?php $this->include_css_files(); ?>
  </head>

  <body class="sign-in">
  
    <div class="sign-in-bar">
	<div class="sign-in">
			<img src="<?php echo $config["base_uri"]; ?>images/logo_small.png" alt="BRCP LOGO" />
			<?php if(isset($alert))
			{?>
				<div class="alert alert-danger"><?php echo $alert; ?></div>
			<?php } ?>
		  <form class="form-signin" role="form" method="post" action="sign_in">
			<h2 class="form-signin-heading">Please sign in</h2>
			<input type="text" class="form-control" name="username" value="<?php if(isset($_POST["username"])) echo $_POST["username"]; ?>" placeholder="Username" required autofocus>
			<input type="password" class="form-control" name="password" placeholder="Password" required>
			<button type="submit" class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
		  </form>

		</div> <!-- sing-in -->
		<?php echo $config["version"]; ?>
	</div><!--sign in bar-->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
