<?php
class Controller
{
	private $name;
	private $notice;
	private $alert;
	
	public function __construct()
	{
		$this->modelRequireer();
	}
	
	/*The following function will just prepare the view for inclusion in any layout views.
	To actually use the template call $this->yieldView() from the layout file, which should be
	found in /views/application/layout.php :)*/
	public function renderLayoutView()
	{
		global $config;
		global $router;
		if(func_num_args() > 0) foreach(func_get_args()[0] as $key => $value) $this->$key = $value;
		$this->viewFolder = $this->get_view_folder();
		if(!isset($this->function)) $this->function = $router->getAction();
		if(isset($this->notice)) $this->notice = $this->notice;
		if(isset($this->alert)) $this->alert = $this->alert;
		if(func_num_args() <= 1 || func_get_args()[1] == null)
		{
			include("../views/Layouts/application.php");
		}
		else
		{
			include("../views/Layouts/".func_get_args()[1].".php");
		}
	}
	
	
	public function get_view_folder()
	{
		global $router;
		if(preg_match("/(.+)Controller/",$router->getController(),$matches))
		{
			return $matches[1];
		}
		else
		{
			return $this->getCallingClass(debug_backtrace());
		}
	}
	
	public function yieldView()
	{
		global $config;
		include("../views/".$this->viewFolder."/".$this->function.".php");
	}
	
	public function renderView()
	{
	    //This function appears to be complete don't touch...
		//NOTE: Variables can be passed in here and they will go to the view...
		if(func_num_args() > 0) foreach(func_get_args()[0] as $key => $value) $this->$key = $value;
		global $config;
		$viewFolder = $this->getCallingClass(debug_backtrace());
		$function = $this->getCallingFunction(debug_backtrace());
		if(isset($this->notice)) $notice = $this->notice;
		if(isset($this->alert)) $alert = $this->alert;
		include("../views/".$viewFolder."/".$function.".php");
	}
	
	//function allows a specific relative path to be used instead of the regular action/controller
	public function renderPathView($path)
	{
		if(!file_exists($path)) trigger_error("Bad Path: File not found by renderView(path)",E_USER_ERROR);
		global $config;
		include($path);
	}
	
	private function modelRequireer()
	{
		//meant to import the correct model file
		$model = $this->constructGetName();
		require("../models/".$model.".php");
	}
	
	private function getCallingClass($debugArray)
	{
		$callers=$debugArray;
		$controller = $callers[1]["class"];
		$viewFolder = preg_replace("/Controller\\z/","",$controller,-1);
		return $viewFolder;
	}
	
	private function getCallingFunction($debugArray)
	{
		$callers = $debugArray;
		$function = $callers[1]["function"];
		return $function;
	}
	
	private function constructGetName()
	{
		//We cannot rely on the same method as getName
		$callers = debug_backtrace();
		$object = get_class($callers[0]["object"]);
		$modelName = preg_replace("/sController\\z/","",$object,-1);
		return $modelName;
	}
	
	public function redirect($url)
	{
		Header("location: $url");
	}
	
	private function load_controller_modules()
	{
		//load modules from /modules/controller_name
	}
	
	public function format()
	{
		global $router;
		return	$router->getFormat();
	}
	
	
	public function before_filter()
	{
		global $router;
		//die("action: ".$router->getAction());
		$args = func_get_args();
		if(func_num_args() == 2)
		{
			if(!is_array($args[1]))
			{
				trigger_error("2nd arg must be an array");
			}
			if(isset($args[1]["except"]))
			{
				foreach($args[1]["except"] as $exception)
				{
					if($exception == $router->getAction())
					{
						return;
					}
				}
			}
			if(isset($args[1]["only"]))
			{
				foreach($args[1]["only"] as $permitted)
				{
					if($permitted == $router->getAction())
					{
						call_user_func([$this,$permitted]);
						return;
					}
				}
			}
		}
		call_user_func([$this,$args[0]]);
	}
	
	
	public function render()
	{
		$args = func_get_args();
		$this->function = $args[0];
		if(count($args)==1)
		{
			call_user_func(array($this,$args[0]));
		}
		if(count($args) == 2)
		{
			if(!is_array($args[1]))
			{
				trigger_error("Second argument of render must be an array");
			}
			foreach($args[1] as $key=>$value)
			{
				$this->$key = $value;
			}
			call_user_func(array($this,$args[0]));
		}
	}
	
	public function include_css_files()
	{
		$this->get_css_files_from_dir("assets");
		$this->get_css_files_from_dir("css");
	}
	
	private function get_css_files_from_dir($directory)
	{
		global $config;
		echo '<style type="text/css">',"\n";
		$fileIterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator("../".$directory),RecursiveIteratorIterator::SELF_FIRST);
		foreach($fileIterator as $fileName=>$fileObj){
			if(strpos($fileName,".css") === false) continue;
			echo "\t",'@import url("'.$this->webize_asset_path($config["base_uri"].$fileName).'");',"\n";
		}
		echo '</style>';
	}
	
	private function webize_asset_path($path)
	{
		$path = preg_replace("/\\\\/",'/',$path);
		$path = preg_replace("/\\/\\.\\.\\//",'/',$path);
		return $path;
	}
}