<h2>New User</h2>
<?php
	if(count($this->user->get_errors()) > 0)
	{
		echo '<div class="alert alert-danger"><b>Errors:</b><ul>';
		foreach($this->user->get_errors() as $error)
		{
			echo "<li>$error</li>";
		}
		echo '</ul></div>';
	}
?>
<form method="post" action="create">
	<table>
		<tr><td>Login:</td><td><input name="username" /></td></tr>
		<tr><td>First Name:</td><td><input name="first_name" /></td></tr>
		<tr><td>Last Name:</td><td><input name="last_name" /></td></tr>
		<tr><td>Email:</td><td><input name="email" type="email" /></td></tr>
		<tr><td>Password:</td><td><input name="password" type="password" /></td></tr>
		<tr><td>Confirm Password:</td><td><input name="confirm_password" type="password" /></td></tr>
		<tr><td>Domain:</td><td><input name="domain" /></td></tr>
		<tr><td>User Level:</td><td><select name="level">
										<option value="1">User</option>
										<option value="2">Administrator</option>
									</select>
							</td></tr>
		
		<tr><td colspan="2"><input type="submit" value="Create" class="btn btn-success"/></td></tr>
	</table>
</form>