<?php $prefix = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
	  $port = isset($_SERVER["SERVER_PORT"]) ? $_SERVER["SERVER_PORT"] : 80;?>
<div class="panel panel-default brcp-panel">
<div class="panel-heading">
<h2>Usage Information</h2>
</div>
<div class="panel-body">
Disk 50% (0.5 GB / 1 GB):
 <div class="progress">
  <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%;">
  </div>
</div>
Bandwidth 100% (0.1 GB / 0.1 GB):
 <div class="progress">
  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
  </div>
</div>
</div>
</div>


<div class="panel panel-default brcp-panel">
<div class="panel-heading">
<h2>Email Administration</h2>
</div>
<div class="panel-body">
	<div class="action_item"><img src="../images/icons/black_envelope.png" alt="Read Email" /><br />Read Email</div>
	<div class="action_item"><img src="../images/icons/black_user.png" alt="user" /><br />Manage Users</div>
<div>
</div>
</div>
</div>

<div class="panel panel-default brcp-panel">
<div class="panel-heading">
<h2>Database Administration</h2>
</div>
<div>
	<div class="action_item"><a href="/phpmyadmin"><img src="../images/icons/black_database.png" /><br />PHPMyAdmin</a></div>
</div>
</div>

<div class="panel panel-default brcp-panel">
<div class="panel-heading">
<h2>Logs</h2>
</div>
<div>
	<div class="action_item"><a href="show_log" target="_blank"><img src="../images/icons/black_book.png" /><br />Rails Log</a></div>
</div>
</div>


<div class="panel panel-default brcp-panel">
<div class="panel-heading">
<h2>Server Administration</h2>
</div>
<div class="panel-body">
<div>
	<div class="action_item"><a href="backup_data"><img src="../images/icons/black_backup.png" /><br />Backup Data</a></div>
	<div class="action_item"><a href="webconsole" target="_blank" href="http://<?php echo $_SERVER['SERVER_ADDR']; ?>:2083" /><img src="../images/icons/black_console.png" alt="Console" /><br />Web Console</a></div>
	<div class="action_item"><a href="../ssl"><img src="../images/icons/black_lock.png" alt="Secure Sockets Layer" /><br />Site SSL</a></div>
	<div class="action_item"><a href="../ssl"><img src="../images/icons/black_globe.png" alt="DNS Settings" /><br />Manage DNS</a></div>
</div>
</div>
</div>

<div class="panel panel-default brcp-panel">
<div class="panel-heading">
<h2>Help &amp; Guides</h2>
</div>
<div class="panel-body">
	<div class="action_item"><a href="../page/help"><img src="../images/icons/black_lifebuoy.png" alt="Life Buoy" /><br />Deploying Rails</a></div>
</div>
</div>
