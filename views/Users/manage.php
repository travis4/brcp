<h2>Manage Users</h2>
<table class="table">
<tr><th>User name</th><th>Domain</th><th></th></tr>
<?php foreach($this->users as $usr)
{
	echo "<tr><td>".$usr["login"]."</td><td>".$usr["domain"]."</td><td>";
	echo '<a href="#" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span> Edit</a> ';
	echo '<a href="#" class="btn btn-primary"><span class="glyphicon glyphicon-download-alt"</span> Back Up</a>';
	echo '<form action="delete" method="post" class="inline-form">';
	echo '<input type="hidden" name="user_id" value="'.$usr["id"].'" />';
	echo "<button type=\"submit\" class=\"btn btn-danger\"><span class=\"glyphicon glyphicon-remove\">&nbsp;Delete</span></button></td></tr>\r\n";
	echo "</form>";
}?>
</table>

<div>
	<a href="./new_user" class="btn btn-success">New User</a>
</div>