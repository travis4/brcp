<h2>Welcome to BRCP!</h2>
<p>At the current time BRCP is a minimalist control panel (though it may not be in the future).  As a result,
this panel controls nothing for the user.  However, we think that the final design is actually quite intuitive.</p>
<h3>Deploying Rails</h3>
<p>Each user gets one Rails application that runs on the system's ruby and one MySQL(MariaDB) database. You must
put your rails application root in the root file of your account.
You may select your rails version by placing it in the gemfile.  To install all of your gems use the command "bundle --path=
vendor/bundle" without quotes.</p>
<h3>Rake</h3>
<p>To use the rake command you must preface it with "bundle exec" without the quotes. For example, to run your
migrations you would use "bundle exec rake db:migrate" without quotes.</p>
<h3>Database</h3>
<p>You have been allocated one MySQL database.  The name of the database is the same as your user name.  Your MySQL
username and password are the same as your username and password when you signed up.</p>
<h3>File Uploads</h3>
<p>BRCP does not support FTP due to security concerns.  Please, use SFTP (avalible in Filezilla) to upload your files
to and from your account.  However, we do not advise that you store your password.</p>
<h3>Restarting Your Application<h3>
<p>To restart your application login via ssh and run the command "touch tmp/restart.txt".</p>
<h3>PHP</h3>
<p>It is possible to run PHP on your account if you do not have a rails application already running.  To use PHP
login to your account via ssh and run the command "mkdir public" then upload any PHP scripts to the public directory
of your account.  The database information is the same as a Rails application.</p>
<h3>DNS Server</h3>
<p>BRCP does not support DNS at this current time.  Please consider something like Amazon Route53 for your DNS needs.</p>
<h3>PHPMyAdmin</h3>
<p>PHPMyAdmin is not supported at this current time, though it is planned.  Please use the rails console, "bundle exec rails c production"
for the easiest way to manipulate MySQL.  It is also possible to login via the command line after logining into your account via
ssh.  To do this simply issue the command "mysql -u [your user name] -p" replacing [your user name] with your actual user name.</p>
<h3>Backups</h3>
<p>If you use the control panel to backup your data the standard Windows unzip will not correctly unzip it.  We recommend using 7-zip.</p>
<h3>Issues</h3>
<p>If you have issues (such as a missing library) please, contact support so they can help you out.  Please, also consider
reporting the issue to the BRCP team so it can be fixed in future releases of BRCP.</p>