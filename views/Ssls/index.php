
<div id="csrModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Generate Certificate Signing Request</h4>
			</div>
			<form action="<?php echo $config["base_uri"];?>/ssl/generate_csr" method="post" id="csrForm">
			<div class="modal-body">
				<div class="col-md-12">
					<div classs="row">
						<label>Common Name:</label>
					</div>
					<div class="row">
						<select name="commonName" class="form-control">
							<option value="<?php echo $this->current_user->domain;?>"><?php echo $this->current_user->domain;?></option>
						</select>
					</div>
					<div class="row">
						<label>Two Letter Country Code:</label>
					</div>
					<div class="row">
						<input name="countryCode" pattern="[AZ}{2}" maxlength="2" placeholder="US"class="form-control" />
					</div>
					<div class="row">
						<label>State/Provience:</label>
					</div>
					<div class="row">
						<input name="state" class="form-control" placeholder="Utah" />
					</div>
					<div class="row">
						<label>Locality (City):</label>
					</div>
					<div class="row">
						<input name="locality" class="form-control" placeholder="Salt Lake City"/>
					</div>
					<div class="row">
						<label>Organization:</label>
					</div>
					<div class="row">
						<input name="organization" class="form-control" placeholder="Orchid Software Ltd."/>
					</div>
					<div class="row">
						<label>Email</label>
					</div>
					<div class="row">
						<input name="email" class="form-control" placeholder="someone@example.com" />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-success">Generate CSR</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			</form>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="col-md-12">
		<div class="row">
			<div class="panel panel-default brcp-panel">
				<div class="panel-heading"><h2>Keys</h2></div>
				<div class="panel-body">
				<div class="col-md-12">
					<div class="row">
						<label>Private Key:</label>
					</div>
					<div class="row">
						<textarea class="form-control" name="private" id="private"><?php echo $this->private; ?></textarea>
					</div>
					<div class="row">
						<label>Public Key:</label>
					</div>
					<div class="row">
						<textarea class="form-control" name="public" id="public">
<?php echo $this->public; ?></textarea>
					</div>
					<div class="row">
						<button class="btn btn-default" id="keyGen">Generate Private & Public Key Pair</button>
					</div>
				</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="panel panel-default brcp-panel">
				<div class="panel-heading"><h2>Certificate</h2></div>
				<div class="panel-body">
					<div class="col-md-12">
						<div class="row">
							<label>Signing Request:</label>
						</div>
						<div class="row">
							<textarea name="csr" id="csr" class="form-control"><?php echo $this->csr; ?></textarea>
						</div>

						<div class="row">
							<button class="btn btn-default" id="csr" data-toggle="modal" data-target="#csrModal">Generate Signing Request</button>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<script>
	$("#keyGen").click(keyGen);
	$("#csrForm").submit(csrForm);
	
	function keyGen()
	{
		$.get("<?php echo $config['base_uri']; ?>/ssl/generate_public_private_keys").success(function(data)
		{
			var json = JSON.parse(data);
			$('#private').val(json.private_key);
			$('#public').val(json.public_key);
		});
	}

	function csrForm(event)
	{
		event.preventDefault();
		$.ajax($(this).attr("action"),
			{
				data: $(this).serialize() + "&privateKey="+$("#private").val(),
				method: "POST"
			}).success(function(data)
			{
				var json = JSON.parse(data);
				$("#csr").val(json.csr);
				$("#csrModal").modal('hide');
			});
	}
</script>
