<!DOCTYPE html>
<html>
<head>
<title>Basic Ruby Control Panel</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<?php $this->include_css_files(); ?>
</head>
<body>
	<header>
	<div>
	<img src="<?php echo $config["base_uri"]; ?>/images/logo_small.png"  alt="logo" />
	<ul>
		<li><a href="<?php echo $config["base_uri"]; ?>user/"><img src="<?php echo $config["base_uri"]; ?>images/icons/white_home.png" alt="home" /></a></li>
		<li><a href="<?php echo $config["base_uri"]; ?>user/sign_out"><img src="<?php echo $config["base_uri"]; ?>images/icons/white_power.png" alt="leave" /></a></li>
	</ul>
	</div></header>
	<div id="content">
		<?php
		if(isset($this->alert)) echo '<div class="alert alert-danger">',$this->alert,'</div>';
		if(isset($this->notice)) echo '<div class="alert alert-success">',$this->notice,'</div>';
		$this->yieldView();
		?>
	</div>
</body>
</html>
