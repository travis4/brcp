CREATE USER 'brcp' IDENTIFIED BY 'ilovebrcp';
CREATE DATABASE brcp;
USE brcp;
CREATE TABLE users(id int(11) AUTO_INCREMENT, login varchar(100), first_name varchar(100), last_name varchar(100), email varchar(100), domain varchar(100), user_level int(11), session_id varchar(100), password varchar(100),PRIMARY KEY(id));
GRANT ALL PRIVILEGES ON brcp.* TO 'brcp'@'localhost' IDENTIFIED BY 'ilovebrcp' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON brcp.* TO 'brcp'@'%' IDENTIFIED BY 'ilovebrcp' WITH GRANT OPTION;
DROP USER ''@'localhost';
