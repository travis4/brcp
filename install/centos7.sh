#!/bin/bash
setenforce 0
yum -y install gcc
yum -y remove mysql
yum -y remove mysql-server
yum -y install gcc make python gcc-c++
yum -y install bison
yum -y install libjpeg
yum -y install libjpeg-devel
yum -y install libpng
yum -y install libpng-devel
yum -y install libxslt-devel
yum -y install httpd
yum -y install wget
yum -y install unzip
yum -y install curl-devel httpd-devel apr-devel apr-util-devel
yum -y install mariadb mariadb-server mysql-devel
yum -y install php php-pdo php-mysql
yum -y install php-mbstring

mkdir /etc/httpd/sites-enabled
wget https://gitlab.com/travis4/brcp/repository/master/archive.tar.gz -O brcp.tar.gz
tar -xvf brcp.tar.gz
rm brcp.tar.gz
mv ./brcp-master* /var/www/html/brcp 
cd /var/www/html/brcp/install/
#Disable SELinux
php disable_selinux.php

yum -y install mariadb-server
service mariadb start
cd /var/www/html/brcp/config/
cp ./database.php.example ./database.php 


chkconfig --add httpd
chkconfig --level 345 httpd on
chkconfig --add mariadb
chkconfig --level 345 mariadb on

#Firewall
firewall-cmd  --zone=public --add-port=4000/tcp --permanent
firewall-cmd --zone=public --add-port=80/tcp --permanent
firewall-cmd --reload

#Move vhost
cp /var/www/html/brcp/install/CentOS5/brcp_vhost.txt /etc/httpd/sites-enabled/brcp

#Populate SQL
mysql -u root < /var/www/html/brcp/install/install.sql
cd /var/www/html/brcp/cpp/
g++ SQLGenerator.cpp -o SQLGenerator
./SQLGenerator
mysql -u root < install.sql

#Install PHP my Admin
wget https://files.phpmyadmin.net/phpMyAdmin/5.0.1/phpMyAdmin-5.0.1-all-languages.zip -O phpmyadmin.zip
unzip phpmyadmin.zip
mv ./phpMyAdmin-5.0.1-all-languages /var/www/html/phpmyadmin

#compile daemons
cd /var/www/html/brcp/cpp
g++ brcpuserd.cpp -o brcpuserd
echo "Moving daemon to /usr/bin/"
mv ./brcpuserd /usr/bin/brcpuserd
echo "Compiling the brcpuserrmd deamon"
g++ brcpuserrmd.cpp -o brcpuserrmd
echo "Moving daemon to /usr/bin/"
mv ./brcpuserrmd /usr/bin/brcpuserrmd
echo "Creating brcp folder in var."
mkdir /var/brcp
chcon -t httpd_sys_content_t /var/brcp/
touch /var/brcp/newuser.list
touch /var/brcp/deluser.list
echo "Moving init script (brcpuserd)"
mv /var/www/html/brcp/install/CentOS5/brcpuserd.init /etc/init.d/brcpuserd
echo "Moving init script (brcpuserrmd)"
mv /var/www/html/brcp/install/CentOS5/brcpuserrmd.init /etc/init.d/brcpuserrmd
chmod +x /etc/init.d/brcpuserd
chmod +x /etc/init.d/brcpuserrmd
chkconfig --add brcpuserd
chkconfig --level 345 brcpuserd on
chckconfig --add brcpuserrmd
chkconfig --level 345 brcpuserrmd on
echo "Changing permissions of /var/brcp/newuser.list"
chmod 0666 /var/brcp/newuser.list
echo "Changing permissions of /var/brcp/deluser.list"
chmod 0666 /var/brcp/deluser.list
echo "Adding ACL to fstab"
cd /var/www/html/brcp/install/CentOS5
php acl_installer.php
mount -o remount /
echo "Protecting BRCP Folder"
chmod -R 0700 /var/www/html/brcp/
setfacl -R -m u:apache:rwx /var/www/html/brcp/
echo "Assigning brcp and phpmyadmin to apache"
chown -R apache /var/www/html/brcp
chown -R apache /var/www/html/phpmyadmin
