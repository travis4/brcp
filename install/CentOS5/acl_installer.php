<?php
	$newFileContent = "";
	$file = fopen("/etc/fstab","r");
	if(!$file) die("Could not open fstab.");
	while(($line = fgets($file)) !== false)
	{
		if(preg_match("/\\s\\/\\s.+defaults/",$line) == 1)
		{
			$defaultsPos = strpos($line,"defaults");
			$line = substr_replace($line,",acl",($defaultsPos+8),0);
			$newFileContent .= $line;
		}
		else
		{
			$newFileContent .= $line;
		}
		
	}
	fclose($file);
	if(file_put_contents("/etc/fstab",$newFileContent) == false)
	{
		die("Could not write to fstab");
	}
	else
	{
		echo "Successfully written to /etc/fstab/";
	}
	
?>