#!/bin/bash
yum -y install gcc
yum -y remove mysql
yum -y remove mysql-server
yum -y install gcc make python gcc-c++
yum -y install bison
yum -y install libjpeg
yum -y install libjpeg-devel
yum -y install libpng
yum -y install libpng-devel
yum -y install libxslt-devel
echo "Installing node"
sudo rpm -Uvh http://dl.fedoraproject.org/pub/epel/5/x86_64/epel-release-5-4.noarch.rpm
sudo yum -y install python26
mkdir -p $HOME/bin
ln -s /usr/bin/python26 $HOME/bin/python
echo 'export PATH=$HOME/bin:$PATH' >> ~/.bashrc
source ~/.bashrc
sudo yum groupinstall "Development Tools"
echo "Allowing Apache to bind to port 4000"
semanage port -a -t http_port_t -p tcp 4000
cd ~
wget http://nodejs.org/dist/v0.10.28/node-v0.10.28.tar.gz
tar -xzf node-v0.10.28.tar.gz
cd node-v0.10.28
./configure
make -j5
make install
echo "symlinking node so Apache+Passenger can find it"
ln -s /usr/local/bin/node /usr/bin/node
echo "installing libyaml"
wget http://pyyaml.org/download/libyaml/yaml-0.1.4.tar.gz
tar xzvf yaml-0.1.4.tar.gz
cd yaml-0.1.4
./configure --prefix=/usr/local
make
make install
#yum -y install libyaml-devel
\curl -sSL https://get.rvm.io | bash -s stable
source /etc/profile.d/rvm.sh
mkdir /etc/httpd/sites-avalible/
mkdir /etc/httpd/sites-enabled/
wget http://goo.gl/ooM7MK -O a2enmod.tar.gz
tar -xvf a2enmod.tar.gz
rm a2enmod.tar.gz
cd gist*
mv ./a2enmod.sh /usr/local/bin/a2enmod.sh
cd ..
rm -rf ./gist*/
cd /usr/local/bin
chmod a=r+x a2enmod.sh
rvm install ruby 2.0.0-p481
echo "Setting Default Ruby to 2.0.0p481"
rvm --default use 2.0.0-p481
gem install bundler
yum -y install curl-devel httpd-devel apr-devel apr-util-devel gd
wget http://goo.gl/7f0wny -O php.tar.gz
tar -xvf php.tar.gz
cd php-5.5.10
yum -y install libxml2-devel libmcrypt-devel libpng-devel
./configure --with-config-file-path=/etc --with-config-file-scan-dir=/etc/php.d  --with-apxs2 --with-mysql --with-mysqli --with-zlib --with-curl --with-libdir=lib --with-openssl --with-pdo-mysql --with-mcrypt  --with-pcre-regex --enable-zip --with-gd --enable-mbstring --with-jpeg-dir=/usr/lib
make clean
make 
make install
#Install suPHP.
echo "Installing suPHP"
wget http://www.suphp.org/download/suphp-0.7.1.tar.gz
tar -xvf suphp-0.7.1.tar.gz
cd suphp-0.7.1
./configure --prefix=/usr
make
make install
#The PHP installler appears to add itself to apache.
chcon -t textrel_shlib_t /usr/lib/httpd/modules/libphp5.so
service httpd restart
cd /etc/httpd/conf.d/
wget http://goo.gl/XricRW -O php.conf
cd ~/
wget http://goo.gl/m23Fd2 -O phpmyadmin.zip
unzip phpmyadmin.zip
mv ./phpMyAdmin-4.1.12-all-languages /var/www/html/phpmyadmin
chcon -v -R -t httpd_sys_content_t /var/www/html/phpmyadmin
yum -y install cmake ncurses-devel
wget https://downloads.mariadb.org/f/mariadb-5.5.40/source/mariadb-5.5.40.tar.gz/from/http%3A/mirror.jmu.edu/pub/mariadb?serve -O mariadb.tar.gz
tar -xvf mariadb.tar.gz
cd mariadb-5.5.40
cmake .
make
make install
cd /usr/local/mysql/
useradd mysql
chown -R mysql /usr/local/mysql/
scripts/mysql_install_db --user=mysql
cd /usr/local/mysql/support-files/
cp mysql.server /etc/init.d/mysql
chmod +x /etc/init.d/mysql
chkconfig --add mysql
chkconfig --level 345 mysql on
ln -s /usr/local/mysql/bin/mysql /usr/bin/mysql
cd /var/www/
chkconfig --add httpd
chkconfig --level 345 httpd on
cd /var/www/html/
wget https://gitlab.pessetto.com/plowdawg/brcp/repository/archive.tar.gz -O brcp.tar.gz --no-check-certificate
tar -xvf brcp.tar.gz
rm brcp.tar.gz
mv ./brcp.git ./brcp
cd /var/www/html/brcp/install/
service mysql start
mysql -u root < install.sql
setsebool -P httpd_can_network_connect_db=1
echo "Allowing apache to read from user's directory"
setsebool -P httpd_enable_homedirs=1
cd /var/www/html/brcp/install/CentOS5SELinuxModule/
echo "Installing module"
semodule -i mariadblocal.pp
semodule -i apacheuser.pp
semodule -i passenger.pp
semodule -i apachezipdump.pp
semodule -i apachewrite2dir.pp
cd /var/www/html/brcp/config/
echo "Copying db config"
cp ./database.php.example ./database.php 
echo "Replacing httpd conf file"
cd /var/www/html/brcp/install/CentOS5
mv ./httpd.conf /etc/httpd/conf/httpd.conf
echo "Installing passenger"
gem install passenger -v 4.0.41
passenger-install-apache2-module
echo "Compiling the SQL Generator for installation"
cd /var/www/html/brcp/cpp
g++ SQLGenerator.cpp -o SQLGenerator
echo "Starting SQL Generator"
./SQLGenerator
echo "Executing generated SQL"
mysql -u root < install.sql
echo "Removing generated sql"
rm install.sql
echo "Compiling the brcpuserd daemon"
cd /var/www/html/brcp/cpp
g++ brcpuserd.cpp -o brcpuserd
echo "Moving daemon to /usr/bin/"
mv ./brcpuserd /usr/bin/brcpuserd
echo "Compiling the brcpuserrmd deamon"
g++ brcpuserrmd.cpp -o brcpuserrmd
echo "Moving daemon to /usr/bin/"
mv ./brcpuserrmd /usr/bin/brcpuserrmd
echo "Creating brcp folder in var."
mkdir /var/brcp
chcon -t httpd_sys_content_t /var/brcp/
touch /var/brcp/newuser.list
touch /var/brcp/deluser.list
echo "Moving init script (brcpuserd)"
mv /var/www/html/brcp/install/CentOS5/brcpuserd.init /etc/init.d/brcpuserd
echo "Moving init script (brcpuserrmd)"
mv /var/www/html/brcp/install/CentOS5/brcpuserrmd.init /etc/init.d/brcpuserrmd
chmod +x /etc/init.d/brcpuserd
chmod +x /etc/init.d/brcpuserrmd
chkconfig --add brcpuserd
chkconfig --level 345 brcpuserd on
chckconfig --add brcpuserrmd
chkconfig --level 345 brcpuserrmd on
echo "Changing permissions of /var/brcp/newuser.list"
chmod 0666 /var/brcp/newuser.list
echo "Changing permissions of /var/brcp/deluser.list"
chmod 0666 /var/brcp/deluser.list
echo "Moving brcp virtual host (listen on port 4000)"
mv /var/www/html/brcp/install/CentOS5/brcp_vhost.txt /etc/httpd/sites-enabled/brcp
echo "Installing bundler"
gem install bundler
echo "Opening port 4000"
iptables -I INPUT -p tcp --dport 4000 -j ACCEPT
iptables -I INPUT -p tcp --dport 80 -j ACCEPT
service iptables save
echo "Changing SELinux policy to allow httpd home folders"
setsebool -P httpd_enable_homedirs=1
echo "Installing ACL"
yum -y install acl
echo "Adding ACL to fstab"
cd /var/www/html/brcp/install/CentOS5
php acl_installer.php
mount -o remount /
echo "Protecting BRCP Folder"
chmod -R 0700 /var/www/html/brcp/
setfacl -R -m u:apache:rwx /var/www/html/brcp/
echo "Assigning brcp and phpmyadmin to apache"
chown -R apache /var/www/html/brcp
chown -R apache /var/www/html/phpmyadmin
echo "Installing ROTE"
wget http://downloads.sourceforge.net/project/rote/rote/rote-0.2.8/rote-0.2.8.tar.gz?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Frote%2F&ts=1403801894&use_mirror=tcpdiag
tar -xvf rote-*.tar.gz
cd rote-*
./configure
make
make install
cd ..
rm -rf rote
echo "Installing Subversion"
yum -y install subversion
echo "Installing AjaxTerm"
cd /usr/share/
wget http://antony.lesuisse.org/software/ajaxterm/files/Ajaxterm-0.10.tar.gz
mkdir ajaxterm
tar -xvf Ajaxterm-*
mv Ajaxterm-*/* ./ajaxterm
rm -rf Ajaxterm-*
echo "Moving vhost file for ajaxterm"
mv /var/www/html/brcp/install/CentOS5/ajaxterm_vhost.txt /etc/httpd/sites-enabled/ajaxterm
echo "Moving init script"
mv /var/www/html/brcp/install/CentOS5/ajaxterm.init /etc/init.d/ajaxterm
chmod +x /etc/init.d/ajaxterm
chkconfig --add ajaxterm
chkconfig --level 345 ajaxterm on
echo "Opening port 8023"
iptables -I INPUT -p tcp --dport 8023 -j ACCEPT
#NEXT STEP SHOULD BE LAST MOVE....
echo "Taking system for a reboot"
reboot